#ifndef UDP_HEADER
#define UDP_HEADER

#include <netdb.h>

int get_address_info(char** args, struct addrinfo* hints, struct addrinfo* servInfo, short receiver);

void set_hints(struct addrinfo* hints, short receiver);

int get_socket(struct addrinfo *servinfo, struct addrinfo *p, short receiver); 

int send_message(char* msg, int socketfd, struct addrinfo* p);

int receive_message(int sockfd, struct sockaddr_storage their_addr); 
#endif // !UDP_HEADER

