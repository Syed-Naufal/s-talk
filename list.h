#ifndef LIST_HEADER
#define LIST_HEADER


typedef struct LIST LIST;
typedef struct NODE NODE;
enum placeInList { beforeList, afterList, inList, uninitialized };

struct LIST {
	struct NODE* head;
	struct NODE* tail;
	struct NODE* current;
	struct LIST* prevList; 
	struct LIST* nextList; 
	enum placeInList place; 
	int listSize;
};

struct NODE {
	NODE* nextNode;
	NODE* prevNode;
	void* data;
};


LIST* ListCreate();

int ListCount(LIST* list);

void* ListFirst(LIST* list);

void* ListLast(LIST* list);

void* ListNext(LIST* list);

void* ListPrev(LIST* list);

void* ListCurr(LIST* list);

int ListAdd(LIST* list, void* item);

int ListInsert(LIST* list, void* item);

int ListAppend(LIST* list, void* item);

int ListPrepend(LIST* list, void* item); 

void* ListRemove(LIST* list); 

void ListConcat(LIST* list1, LIST* list2);

void ListFree(LIST* list, void (*itemFree)(NODE*));

void* ListTrim(LIST* list); \

void* ListSearch( LIST* list, int (*comparator)(void*, void*), void* comparisonArg); 

void itemFree(NODE* node);

int comparisonFunction(void* arg1, void* arg2); 

#endif // !LIST_HEADER