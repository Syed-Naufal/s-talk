/*
UDP related code follows Beej's Tutorial
*/
#define _POSIX_C_SOURCE 200112L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "thread_funcs.h"
#include "list.h"

#define MYPORT "4950"	// the port users will be connecting to

#define MAXBUFLEN 100

// get sockaddr, IPv4 or IPv6:
void* get_in_addr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char** argv)
{
	/*
	1: my port num
	2: remote machine name
	3: remote port num
	*/
	int myPortNum = atoi(argv[1]);
	int remotePortNum = atoi(argv[3]);
	char* remMachineName = malloc(strlen(argv[2]));
	strcpy(remMachineName, argv[2]);
	printf("%d %s %d\n", myPortNum, remMachineName, remotePortNum);

	/*set up for sending */
	int remSocketfd;
	struct addrinfo remHints, * remServinfo, * remP;
	int rv;
	int numbytes;
	LIST* sendListHandle = ListCreate();

	memset(&remHints, 0, sizeof remHints);
	remHints.ai_family = AF_INET;
	remHints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo(remMachineName, argv[3], &remHints, &remServinfo)) != 0) {
		printf("getaddrinfo returnval %d\n", rv);
		return 1;
	}


	for (remP = remServinfo; remP != NULL; remP = remP->ai_next) {
		if ((remSocketfd = socket(remP->ai_family, remP->ai_socktype, remP->ai_protocol)) == -1) {
			perror("talker: socket");
			continue;
		}
		break;
	}

	if (remP == NULL) {
		fprintf(stderr, "talker: failed to create/bind socket\n");
		return 2;
	}

	/*set up for receiving*/
	int mySocketfd;
	struct addrinfo myHints, * myServinfo, * p;
	struct sockaddr_storage their_addr;
	socklen_t addr_len;
	char s[INET6_ADDRSTRLEN];
	LIST* recvListHandle = ListCreate();

	memset(&myHints, 0, sizeof myHints);
	myHints.ai_family = AF_INET; // set to AF_INET to force IPv4
	myHints.ai_socktype = SOCK_DGRAM;
	myHints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, argv[1], &myHints, &myServinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for (p = myServinfo; p != NULL; p = p->ai_next) {
		if ((mySocketfd = socket(p->ai_family, p->ai_socktype,
			p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

		if (bind(mySocketfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(mySocketfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	/*set up data structures for sending a receiving*/
	struct sender_data sender;
	sender.listHandle = sendListHandle;
	sender.p = remP;
	sender.socketfd = remSocketfd;

	struct receiver_data receiver;
	receiver.listHandle = recvListHandle;
	receiver.theirAddr = &their_addr;
	receiver.socketfd = mySocketfd;

	printf("listener: waiting to recvfrom...\n");
	

	numbytes = receive_message(&receiver);

	printf("listener: got packet from %s\n",
		inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr*) & their_addr), s, sizeof s));
	printf("listener: packet is %d bytes long\n", numbytes);
	printf("before listHandle print\n");

	print_message(recvListHandle);
	printf("after listHandle print\n");

	freeaddrinfo(remServinfo);
	freeaddrinfo(myServinfo);
	close(remSocketfd);
	close(mySocketfd);
	free(remMachineName);
	printf("after close socket\n");

	return 0;
}