#define _POSIX_C_SOURCE 200112L
#include "list.h" 
#include <stdio.h> 
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "udp.h"
#define TRUE 1 
#define MAXBUFLEN 100
char buf[MAXBUFLEN];
/* UDP code taken from Beej's guide from the tutorials 

*/

void* get_in_addr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int get_address_info(char** args, struct addrinfo *hints, struct addrinfo *servInfo, short receiver) {
	int rv; 
	if (receiver == TRUE) {
		if ((rv = getaddrinfo(NULL, args[1], hints, servInfo)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		}
	}
	else {
		printf("%s %s", args[2], args[3]);
		if ((rv = getaddrinfo(args[2], args[3], hints, servInfo)) != 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		}
	}
	return rv; 
}

void set_hints(struct addrinfo *hints, short receiver) {
	memset(hints, 0, sizeof hints);
	hints->ai_family = AF_INET;
	hints->ai_socktype = SOCK_DGRAM;
	if (receiver == TRUE) {
		hints->ai_flags = AI_PASSIVE; // use my IP
	}
	
}

int get_socket(struct addrinfo *servInfo, struct addrinfo *p, short receiver) {
	int socketfd; 
	if (receiver == TRUE) {
		for (p = servInfo; p != NULL; p = p->ai_next) {
			if ((socketfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
				perror("listener: socket");
				continue;
			}

			if (bind(socketfd, p->ai_addr, p->ai_addrlen) == -1) {
				close(socketfd);
				perror("listener: bind");
				continue;
			}

			break;
		}

		if (p == NULL) {
			fprintf(stderr, "talker: failed to create/bind socket\n");
			return -1;
		}
	} 
	else {
		for (p = servInfo; p != NULL; p = p->ai_next) {
			if ((socketfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
				perror("talker: socket");
				continue;
			}
			break;
		}

		if (p == NULL) {
			fprintf(stderr, "talker: failed to create socket\n");
			return -1;
		}
	}
	
	return socketfd; 
}

int send_message(char* msg, int socketfd, struct addrinfo *p) {
	int numbytes; 
	if ((numbytes = sendto(socketfd, msg, strlen(msg), 0, p->ai_addr, p->ai_addrlen)) == -1) {
		perror("talker: sendto");
	}
	return numbytes;
}

int receive_message(int sockfd, struct sockaddr_storage their_addr) {
	int numbytes; 
	socklen_t addr_len;
	addr_len = sizeof their_addr;
	char s[INET6_ADDRSTRLEN];
	if ((numbytes = recvfrom(sockfd, buf, MAXBUFLEN - 1, 0,
		(struct sockaddr*) & their_addr, &addr_len)) == -1) {
		perror("recvfrom");
		exit(1);
	}

	printf("listener: got packet from %s\n",
		inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr*) & their_addr),
			s, sizeof s));
	printf("listener: packet is %d bytes long\n", numbytes);
	buf[numbytes] = '\0';
	printf("listener: packet contains \"%s\"\n", buf);
	return numbytes;
}



/*for sender*/
int socketfd;
struct addrinfo hints, * servInfo, * p;
int rv;
int numbytes;

set_hints(&hints, 0);
printf("after hints\n");
rv = get_address_info(argv, &hints, servInfo, 0);
printf("after addrinfo\n");
socketfd = get_socket(servInfo, p, 0);
printf("after socket\n");
char* message = "hello from sender";
send_message(message, socketfd, p);
printf("after sender\n");
