# S-Talk
## Description: 
A simple chatting program to facilitate messaging between two different terminals. Program achieves this through the use of kernel level Unix system calls and UDP. 

To run program call ./s-talk [my port #] [remote machine name] [remote port #]

Completed February 2020 for CMPT 300. 
